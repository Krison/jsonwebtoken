﻿using System.Reflection;

namespace JsonWebTokens.Presentation;

public static class AssemblyReferance
{
    public static Assembly Assembly = typeof(AssemblyReferance).Assembly;
}
