﻿using JsonWebTokens.Application.Members.Login;
using JsonWebTokens.Application.Statistics.GetRepositoriesStatistics;
using JsonWebTokens.Domain.Shared.Validation;
using JsonWebTokens.Presentation.Abstractions;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace JsonWebTokens.Presentation.Controllers;

[Route("Api/Statistics")]
public class StatisticsController : ApiController
{
    public StatisticsController(ISender sender) : base(sender)
    {
    }

    [HttpGet("GetRepositoryById")]
    public async Task<IActionResult> GetRepositoriesStatistics(
        string token,
        long id,
        CancellationToken cancellationToken)
    {
        var query = new GetRepositoryStatisticsByIdQuery(token, id);

        Result<GetRepositoryStatisticsResponse> response = await Sender.Send(
            query,
            cancellationToken);

        if (response.IsFailure)
        {
            return HandleFailure(response);
        }

        return Ok(response.Value);
    }

    [HttpGet("GetAllRepositoriesByOwner")]
    public async Task<IActionResult> GetRepositoriesStatistics(
        string token,
        string owner,
        CancellationToken cancellationToken)
    {
        var query = new GetRepositoriesStatisticsByOwnerQuery(token, owner);

        Result<GetRepositoriesStatisticsResponse> response = await Sender.Send(
            query,
            cancellationToken);

        if (response.IsFailure)
        {
            return HandleFailure(response);
        }

        return Ok(response.Value);
    }
}
