﻿using JsonWebTokens.Application.Members.Login;
using JsonWebTokens.Domain.Shared.Validation;
using JsonWebTokens.Presentation.Abstractions;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace JsonWebTokens.Presentation.Controllers;

[Route("Api/Members")]
public sealed class MemberController : ApiController
{
    public MemberController(ISender sender) : base(sender)
    {
    }

    [HttpPost("Login")]
    public async Task<IActionResult> LoginMember(
        [FromBody] LoginRequest request,
        CancellationToken cancellationToken)
    {
        var command = new LoginCommand(request.UserName, request.Password);

        Result<string> tokenResult = await Sender.Send(
            command,
            cancellationToken);

        if (tokenResult.IsFailure)
        {
            return HandleFailure(tokenResult);
        }

        return Ok(tokenResult.Value);
    }
}
