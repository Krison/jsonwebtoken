﻿using System.Reflection;

namespace JsonWebTokens.Infrastructure
{
    public static class AssemblyReference
    {
        public static Assembly Assembly = typeof(AssemblyReference).Assembly;
    }
}