﻿using JsonWebTokens.Application.Abstractions;
using JsonWebTokens.Domain.Entities;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace JsonWebTokens.Infrastructure.Authentication;

public sealed class JsonWebTokenProvider : IJsonWebTokenProvider
{
    private readonly JsonWebTokenOptions _options;

    public JsonWebTokenProvider(IOptions<JsonWebTokenOptions> options)
    {
        _options = options.Value;
    }

    public string Generate(Member member)
    {
        var clames = new Claim[]
        {
            new(JwtRegisteredClaimNames.Sub, member.Id.ToString()),
            new(JwtRegisteredClaimNames.Name, member.UserName),
            new(JwtRegisteredClaimNames.AuthTime, DateTime.Now.AddHours(1).ToString())
        };

        var signingCredentials = new SigningCredentials(
            new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes(_options.SecretKey)), 
                SecurityAlgorithms.HmacSha512);

        var token = new JwtSecurityToken(
            _options.Issuer, 
            _options.Audience, 
            clames, 
            null, 
            DateTime.Now.AddHours(1), 
            signingCredentials);

        var tokenValue = new JwtSecurityTokenHandler().WriteToken(token);

        return tokenValue;
    }
}
