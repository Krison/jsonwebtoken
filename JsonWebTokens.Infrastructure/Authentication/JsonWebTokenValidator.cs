﻿using JsonWebTokens.Application.Abstractions;
using JsonWebTokens.Domain.Errors;
using JsonWebTokens.Domain.Shared.Validation;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace JsonWebTokens.Infrastructure.Authentication;

public sealed class JsonWebTokenValidator : IJsonWebTokenValidator
{
    private readonly JsonWebTokenOptions _options;

    public JsonWebTokenValidator(IOptions<JsonWebTokenOptions> options)
    {
        _options = options.Value;
    }

    public Result<bool> ValidateToken(string token)
    {
        var key = Encoding.UTF8.GetBytes(_options.SecretKey);

        SecurityToken? securityToken;
        try
        {
            new JwtSecurityTokenHandler().ValidateToken(
            token,
            new TokenValidationParameters
            {
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                ClockSkew = TimeSpan.Zero
            },
            out securityToken);
        }
        catch
        {
           return Result.Failure<bool>(DomainErrors.ValidationErrors.InvalidToken);
        }
        
        return Result.Success(securityToken is not null);
    }
}
