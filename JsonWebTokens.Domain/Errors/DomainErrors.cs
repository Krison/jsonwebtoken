﻿using JsonWebTokens.Domain.Shared.Validation;

namespace JsonWebTokens.Domain.Errors;

public static class DomainErrors
{
    public static class ValidationErrors
    {
        public static readonly Error InvalidToken = new Error("Token.Invalid", "Provided token is invalid!"); 
    }
}
