﻿using JsonWebTokens.Domain.Entities;

namespace JsonWebTokens.Domain.Repositories;

public interface IStatisticsRepository
{
    Task<IReadOnlyCollection<Statistic>> GetRepositoriesStatisticsByOwner(string owner);
    Task<Statistic> GetRepositoryById(long id);
}
