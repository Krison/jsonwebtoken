﻿using JsonWebTokens.Domain.Entities;

namespace JsonWebTokens.Domain.Repositories;

public interface IMemberRepository
{
    Task<Member?> GetMember(string userName, string password);
}
