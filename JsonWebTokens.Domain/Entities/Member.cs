﻿namespace JsonWebTokens.Domain.Entities;

public class Member
{
    public Guid Id { get; set; }
    public string UserName { get; set; }
    public string Password { get; set; }
}
