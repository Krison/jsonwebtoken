﻿using JsonWebTokens.Application.Abstractions;
using JsonWebTokens.Application.Abstractions.Messaging;
using JsonWebTokens.Domain.Errors;
using JsonWebTokens.Domain.Repositories;
using JsonWebTokens.Domain.Shared.Validation;

namespace JsonWebTokens.Application.Statistics.GetRepositoriesStatistics;

internal sealed class GetRepositoriesStatisticsByOwnerQueryHandler : IQueryHandler<GetRepositoriesStatisticsByOwnerQuery, GetRepositoriesStatisticsResponse>
{
    private readonly IStatisticsRepository _statisticsRepository;
    private readonly IJsonWebTokenValidator _tokenValidator;

    public GetRepositoriesStatisticsByOwnerQueryHandler(IStatisticsRepository statisticsRepository, IJsonWebTokenValidator tokenValidator)
    {
        _statisticsRepository = statisticsRepository;
        _tokenValidator = tokenValidator;
    }

    public async Task<Result<GetRepositoriesStatisticsResponse>> Handle(GetRepositoriesStatisticsByOwnerQuery request, CancellationToken cancellationToken)
    {
        var token = request.Token;

        if (token is null) 
        {
            return Result.Failure<GetRepositoriesStatisticsResponse>(DomainErrors.ValidationErrors.InvalidToken);
        }

        if (string.IsNullOrWhiteSpace(token)) 
        {
            return Result.Failure<GetRepositoriesStatisticsResponse>(DomainErrors.ValidationErrors.InvalidToken);
        }

        var isTokenValid = _tokenValidator.ValidateToken(token);

        if (isTokenValid.IsFailure)
        {
            return Result.Failure<GetRepositoriesStatisticsResponse>(isTokenValid.Error);
        } 

        if (!isTokenValid.Value)
        {
            return Result.Failure<GetRepositoriesStatisticsResponse>(DomainErrors.ValidationErrors.InvalidToken);
        }

        var statistics = await _statisticsRepository.GetRepositoriesStatisticsByOwner(request.Owner);

        return new GetRepositoriesStatisticsResponse(statistics.ToArray());
    }
}
