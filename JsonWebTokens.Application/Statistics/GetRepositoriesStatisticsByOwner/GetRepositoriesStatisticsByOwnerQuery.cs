﻿using JsonWebTokens.Application.Abstractions.Messaging;

namespace JsonWebTokens.Application.Statistics.GetRepositoriesStatistics;

public record GetRepositoriesStatisticsByOwnerQuery(string Token, string Owner) : IQuery<GetRepositoriesStatisticsResponse>;
