﻿using JsonWebTokens.Domain.Entities;

namespace JsonWebTokens.Application.Statistics.GetRepositoriesStatistics;

public record GetRepositoriesStatisticsResponse(Statistic[] Statistics);
