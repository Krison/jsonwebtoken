﻿using JsonWebTokens.Application.Abstractions;
using JsonWebTokens.Application.Abstractions.Messaging;
using JsonWebTokens.Domain.Errors;
using JsonWebTokens.Domain.Repositories;
using JsonWebTokens.Domain.Shared.Validation;

namespace JsonWebTokens.Application.Statistics.GetRepositoriesStatistics;

internal sealed class GetRepositoryStatisticsByIdQueryHandler : IQueryHandler<GetRepositoryStatisticsByIdQuery, GetRepositoryStatisticsResponse>
{
    private readonly IStatisticsRepository _statisticsRepository;
    private readonly IJsonWebTokenValidator _tokenValidator;

    public GetRepositoryStatisticsByIdQueryHandler(IStatisticsRepository statisticsRepository, IJsonWebTokenValidator tokenValidator)
    {
        _statisticsRepository = statisticsRepository;
        _tokenValidator = tokenValidator;
    }

    public async Task<Result<GetRepositoryStatisticsResponse>> Handle(GetRepositoryStatisticsByIdQuery request, CancellationToken cancellationToken)
    {
        var token = request.Token;

        if (token is null) 
        {
            return Result.Failure<GetRepositoryStatisticsResponse>(DomainErrors.ValidationErrors.InvalidToken);
        }

        if (string.IsNullOrWhiteSpace(token)) 
        {
            return Result.Failure<GetRepositoryStatisticsResponse>(DomainErrors.ValidationErrors.InvalidToken);
        }

        var isTokenValid = _tokenValidator.ValidateToken(token);

        if (isTokenValid.IsFailure)
        {
            return Result.Failure<GetRepositoryStatisticsResponse>(isTokenValid.Error);
        } 

        if (!isTokenValid.Value)
        {
            return Result.Failure<GetRepositoryStatisticsResponse>(DomainErrors.ValidationErrors.InvalidToken);
        }

        var statistic = await _statisticsRepository.GetRepositoryById(request.Id);

        return new GetRepositoryStatisticsResponse(statistic);
    }
}
