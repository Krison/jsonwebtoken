﻿using JsonWebTokens.Application.Abstractions.Messaging;

namespace JsonWebTokens.Application.Statistics.GetRepositoriesStatistics;

public record GetRepositoryStatisticsByIdQuery(string Token, long Id) : IQuery<GetRepositoryStatisticsResponse>;
