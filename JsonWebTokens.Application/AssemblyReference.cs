﻿using System.Reflection;

namespace JsonWebTokens.Application;

public static class AssemblyReference
{
    public static Assembly Assembly = typeof(AssemblyReference).Assembly;
}
