﻿using JsonWebTokens.Domain.Shared.Validation;

namespace JsonWebTokens.Application.Abstractions;

public interface IJsonWebTokenValidator
{
    Result<bool> ValidateToken(string token);
}
