﻿using JsonWebTokens.Domain.Shared.Validation;
using MediatR;

namespace JsonWebTokens.Application.Abstractions.Messaging;

public interface IQueryHandler<TQuery, TResponse> : IRequestHandler<TQuery, Result<TResponse>>
    where TQuery : IQuery<TResponse>
{
}