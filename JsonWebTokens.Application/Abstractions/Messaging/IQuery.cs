﻿using JsonWebTokens.Domain.Shared.Validation;
using MediatR;

namespace JsonWebTokens.Application.Abstractions.Messaging;

public interface IQuery<TResponse> : IRequest<Result<TResponse>>
{
}