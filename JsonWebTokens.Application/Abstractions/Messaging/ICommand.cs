﻿using JsonWebTokens.Domain.Shared.Validation;
using MediatR;

namespace JsonWebTokens.Application.Abstractions.Messaging;

public interface ICommand : IRequest<Result>
{
}

public interface ICommand<TResponse> : IRequest<Result<TResponse>> 
{
}
