﻿using JsonWebTokens.Domain.Entities;

namespace JsonWebTokens.Application.Abstractions;

public interface IJsonWebTokenProvider
{
    string Generate(Member member);
}
