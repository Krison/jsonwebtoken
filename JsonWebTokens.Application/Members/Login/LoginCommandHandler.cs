﻿using JsonWebTokens.Application.Abstractions;
using JsonWebTokens.Application.Abstractions.Messaging;
using JsonWebTokens.Domain.Entities;
using JsonWebTokens.Domain.Repositories;
using JsonWebTokens.Domain.Shared.Validation;

namespace JsonWebTokens.Application.Members.Login;
internal sealed class LoginCommandHandler : ICommandHandler<LoginCommand, string>
{
    private readonly IMemberRepository _memberRepository;
    private readonly IJsonWebTokenProvider _jsonWebTokenProvider;

    public LoginCommandHandler(IMemberRepository memberRepository, IJsonWebTokenProvider jsonWebTokenProvider)
    {
        _memberRepository = memberRepository;
        _jsonWebTokenProvider = jsonWebTokenProvider;
    }

    public async Task<Result<string>> Handle(LoginCommand request, CancellationToken cancellationToken)
    {
        Member? member = await _memberRepository.GetMember(request.UserName, request.Password);
        
        if (member == null) 
        {
            return Result.Failure<string>(new Error(
            "Member.InvalidCredentials",
            "The provided credentials are invalid"));
        }

        var token = _jsonWebTokenProvider.Generate(member);

        return token;
    }
}
