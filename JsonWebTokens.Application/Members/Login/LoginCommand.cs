﻿using JsonWebTokens.Application.Abstractions.Messaging;

namespace JsonWebTokens.Application.Members.Login;

public record LoginCommand(string UserName, string Password) : ICommand<string>;