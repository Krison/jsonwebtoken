﻿namespace JsonWebTokens.Application.Members.Login;

public record LoginRequest(string UserName, string Password);
