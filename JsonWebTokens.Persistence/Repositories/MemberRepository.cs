﻿using JsonWebTokens.Domain.Entities;
using JsonWebTokens.Domain.Repositories;

namespace JsonWebTokens.Persistence.Repositories;

public class MemberRepository : IMemberRepository
{
    private readonly IReadOnlyCollection<Member> _members = 
        new List<Member> { 
            new Member { 
                Id = Guid.NewGuid(), 
                UserName = "Administrator", 
                Password = "Password"
            }
        }.AsReadOnly();

    public MemberRepository()
    {
    }

    public async Task<Member?> GetMember(string userName, string password)
    {
        var member = _members.FirstOrDefault(x => x.UserName == userName && x.Password == password);

        return member;
    }
}
