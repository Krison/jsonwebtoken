﻿using JsonWebTokens.Domain.Entities;
using JsonWebTokens.Domain.Repositories;
using Newtonsoft.Json.Linq;

namespace JsonWebTokens.Persistence.Repositories;

public class StatisticsRepository : IStatisticsRepository
{
    private readonly IHttpClientFactory _httpClient;

    public StatisticsRepository(IHttpClientFactory httpClient)
    {
        _httpClient = httpClient;
    }

    public async Task<IReadOnlyCollection<Statistic>> GetRepositoriesStatisticsByOwner(string owner)
    {
        var httpClient = _httpClient.CreateClient("GitHub");

        HttpResponseMessage result = await httpClient.GetAsync($"users/{owner}/repos");

        string resultString = await result.Content.ReadAsStringAsync();

        IReadOnlyCollection<Statistic> statistics = MapToStatisticsCollection(resultString);
        
        return statistics;
    }

    public async Task<Statistic> GetRepositoryById(long id)
    {
        var httpClient = _httpClient.CreateClient("GitHub");

        HttpResponseMessage result = await httpClient.GetAsync($"repositories/{id}");

        string resultString = await result.Content.ReadAsStringAsync();

        Statistic statistic = MapToStatistic(resultString);

        return statistic;
    }

    private Statistic MapToStatistic(string resultString)
    {
        JObject? jObject = JObject.Parse(resultString);

        JObject? ownerObject = jObject.Value<JObject>("owner");

        var statistic = new Statistic
        {
            Id = jObject.Value<long>("id")!,
            Owner = ownerObject?.Value<string>("login") ?? string.Empty,
            Name = jObject.Value<string>("name") ?? string.Empty,
            Description = jObject.Value<string>("description") ?? string.Empty,
            CreatedAt = jObject.Value<DateTime>("created_at"),
            UpdatedAt = jObject.Value<DateTime>("updated_at"),
            DefaultBranch = jObject.Value<string>("default_branch") ?? string.Empty,
        };

        return statistic;
    }

    private IReadOnlyCollection<Statistic> MapToStatisticsCollection(string resultString)
    {
        JArray resultArray = JArray.Parse(resultString);

        var listOfStatistics = new List<Statistic>();

        foreach (JToken token in resultArray)
        {
            JObject? ownerJObject = token.Value<JObject>("owner");

            var statistic = new Statistic
            {
                Id = token.Value<long>("id")!,
                Owner = ownerJObject?.Value<string>("login") ?? string.Empty,
                Name = token.Value<string>("name") ?? string.Empty,
                Description = token.Value<string>("description") ?? string.Empty,
                CreatedAt = token.Value<DateTime>("created_at"),
                UpdatedAt = token.Value<DateTime>("updated_at"),
                DefaultBranch = token.Value<string>("default_branch") ?? string.Empty,
            };

            listOfStatistics.Add(statistic);
        }

        return listOfStatistics.AsReadOnly();
    }
}
