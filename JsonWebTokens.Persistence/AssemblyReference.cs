﻿using System.Reflection;

namespace JsonWebTokens.Persistence;

public static class AssemblyReference
{
    public static Assembly Assembly = typeof(AssemblyReference).Assembly;
}
