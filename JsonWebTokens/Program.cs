using JsonWebTokens.OptionsSetup;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Net.Http.Headers;
using System.Net.Http;

var builder = WebApplication.CreateBuilder(args);

builder.Services.Scan(selector => selector
    .FromAssemblies(
        JsonWebTokens.Persistence.AssemblyReference.Assembly,
        JsonWebTokens.Infrastructure.AssemblyReference.Assembly)
    .AddClasses()
    .AsImplementedInterfaces()
    .WithScopedLifetime());

builder.Services.AddMediatR(config =>
{
    config.RegisterServicesFromAssembly(JsonWebTokens.Application.AssemblyReference.Assembly);
});

builder.Services.AddControllers()
    .AddApplicationPart(JsonWebTokens.Presentation.AssemblyReferance.Assembly);

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.ConfigureOptions<JsonWebTokenOptionsSetup>();
builder.Services.ConfigureOptions<JsonWebTokenBearerOptionsSetup>();

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer();

builder.Services.AddHttpClient("GitHub", config =>
{
    config.BaseAddress = new Uri(builder.Configuration["HttpClient:BaseAddress"]!);
    config.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue("StatCounter", "0.1"));
    config.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
});

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.Run();
